package com.upv.practica06;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends ActionBarActivity {

    private EditText nombre, correo, telefono;
    private Button guardar, mostrar;
    private int data_block = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        guardar = (Button)findViewById(R.id.btnGuardar);
        mostrar = (Button)findViewById(R.id.btnMostrar);
        nombre = (EditText)findViewById(R.id.txtNombre);
        correo = (EditText)findViewById(R.id.txtCorreo);
        telefono = (EditText)findViewById(R.id.txtTelefono);
    }

    public void guardar(View view)
    {
        String message = nombre.getText().toString() + " "+correo.getText().toString() + " "+ telefono.getText().toString();

        try{

            FileOutputStream fou = openFileOutput("text.txt",MODE_APPEND);
            OutputStreamWriter osw = new OutputStreamWriter(fou);
            try{
                osw.write(message);
                osw.flush();
                osw.close();
                Toast.makeText(getBaseContext(), "Dato guardado", Toast.LENGTH_LONG).show();

            }catch(IOException e)
            {
                e.printStackTrace();
            }
        }catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }

        nombre.setText("");
        correo.setText("");
        telefono.setText("");

    }

    public void mostrar(View view)
    {
        try{
            FileInputStream fis = openFileInput("text.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            char[] data = new char[data_block];
            String final_data="";
            int size;
            try{
                while((size = isr.read(data))>0)
                {
                    String read_data = String.valueOf(data,0,size);
                    final_data += read_data;
                    data = new char[data_block];
                }
                Toast.makeText(getBaseContext(), "Message: "+ final_data, Toast.LENGTH_LONG).show();
            } catch(IOException e){
                e.printStackTrace();
            }
        }catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
